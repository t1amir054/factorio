#ifndef _PLAYER_H_
#define _PLAYER_H_

#include "obj/Object.h"
#include "obj/fig/Figure.h"

#define PLAYER_VERTEX_SHADER_PATH "Player.vs"
#define PLAYER_FRAGMENT_SHADER_PATH "Player.fs"

namespace obj {
	class Player : public Figure {
	public:
		Player(glm::mat4* model, glm::mat4* view, glm::mat4* projection, glm::vec3* camPos);
		~Player();
	
		void draw();
		void update();

		void loadShaders();
		void loadTextures();

	private:
		unsigned int _VBO, _VAO;
		Shader* _shader;

		unsigned int _texture;
		glm::vec3* _camPos;
	};
}

#endif