#ifndef _FIGURE_H_
#define _FIGURE_H_

#include "obj/Object.h"

namespace obj {
	class Figure : public Object {
	public:
		Figure(glm::mat4* model, glm::mat4* view, glm::mat4* projection);
		~Figure();
	
	protected:
		
	private:

	};
}

#endif