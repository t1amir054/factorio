#ifndef _DYOBJ_H_
#define _DYOBJ_H_

#include "obj/Object.h"

namespace obj {
	class Dyobj : public Object {
	public:
		Dyobj(glm::mat4* model, glm::mat4* view, glm::mat4* projection);
		~Dyobj();

		//virtual render();
		//virtual update();

	protected:
		//glm::vec3 _vel;

	private:

	};
}

#endif