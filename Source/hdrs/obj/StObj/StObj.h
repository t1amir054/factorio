#ifndef _STOBJ_H_
#define _STOBJ_H_

#include "obj/Object.h"

namespace obj {
	class Stobj : public Object {
	public:
		Stobj(glm::mat4* model, glm::mat4* view, glm::mat4* projection);
		~Stobj();

		//virtual render();
		//virtual update();

	protected:
		

	private:

	};
}

#endif