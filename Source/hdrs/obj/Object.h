#ifndef _OBJECT_H_
#define _OBJECT_H_

#include "Header.h"

namespace obj {
	class Object {
	public:
		Object(glm::mat4* model, glm::mat4* view, glm::mat4* projection);
		~Object();
		
		//virtual void render();
		//virtual void update();

	protected:
		glm::vec3 _pos;

		glm::mat4* _model;
		glm::mat4* _view;
		glm::mat4* _projection;

	private:

	};
}

#endif