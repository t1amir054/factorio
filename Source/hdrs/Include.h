#ifndef _INCLUDE_H_
#define _INCLUDE_H_

#include <iostream>
#include <fstream>
#include <sstream>

#include <string>
#include <vector>
#include <map>

#include <exception>

//opengl libraries
#include <glad/glad.h>
#include <GLFW/glfw3.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

#include "exception.h"

#include "graphics/window.h"
#include "graphics/Shader.h"
#include "graphics/Chunk.h"

//temp:

#endif