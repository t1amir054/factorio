#ifndef _CHUNK_H_
#define _CHUNK_H_

#include "Header.h"
#include "graphics/stb_image.h"

class Shader;

class Chunk {
public:
	Chunk(int x, int y, Shader* shader, int seed = -1);
	~Chunk();

	void draw();

	int _x;
	int _y;
	u_int8_t _tiles[CHUNK_SIZE][CHUNK_SIZE];

private:
	void generate(int seed = -1);
	long int _seed;

	Shader* _shader;

	unsigned int _quadVAO;
	unsigned int _quadVBO;
	unsigned int _instanceVBO;
};

#endif