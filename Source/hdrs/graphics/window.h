#ifndef _WINDOW_H_
#define _WINDOW_H_

#include "Header.h"

namespace ogl {
	void gladInit();
	void oglinit();
	void oglTerminate();
	
	void openWindow(GLFWwindow** pwindow, glm::fvec3* posOffset);
	void generateViewports(int width, int height);
	void framebuffer_size_callback(GLFWwindow* window, int width, int height);

	void processInput(GLFWwindow* window);
	void scroll_callback(GLFWwindow* window, double xoffset, double yoffset);
}

#endif