#ifndef _MAP_H_
#define _MAP_H_

#include "Header.h"
#include "graphics/Chunk.h"

class Map {
public:
	Map(glm::mat4* model, glm::mat4* view, glm::mat4* projection, glm::vec3* position);
	~Map();

	void loadTextures();

	void loadChunkByLocation(glm::vec3 position);
	bool isChunkLoaded(int x, int y);
	void removeLoadedChunks();

	void update();
	void render();

private:
	std::vector<Chunk*> _chunks;

	Shader* _shader;
	unsigned int _textures[6];
	
	glm::mat4* _model;
	glm::mat4* _view;
	glm::mat4* _projection;
	glm::vec3* _position;
};

#endif