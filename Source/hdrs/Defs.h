#ifndef _DEFS_H_
#define _DEFS_H_

#define WIN_HEIGHT 600
#define WIN_WIDTH  800

typedef enum t_tiles {
    Blank = 0,
    Dirt,
    Water,
    TopLeftEdge,
    TopRightEdge,
    BotLeftEdge,
    BotRightEdge,
} t_tiles;

#define MAX_RENDER_DISTANCE 3

#define CHUNK_SIZE 32
#define INDICES_SIZE (6 * (CHUNK_SIZE - 1) * (CHUNK_SIZE - 1))

#endif