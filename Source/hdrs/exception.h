#ifndef _EXCEPTION_H_
#define _EXCEPTION_H_

#include <iostream>

namespace exc {
	class exception {
	public:
		exception(std::string err = "unknown error occured", std::string info = "");

		std::string what() const throw();
	
	private:
		std::string _err;
	};
}

#endif