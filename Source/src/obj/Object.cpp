#include "obj/Object.h"

namespace obj {
	Object::Object(glm::mat4* model, glm::mat4* view, glm::mat4* projection) {
		this->_model = model;
		this->_view = view;
		this->_projection = projection;
	}
	
	Object::~Object() {
		
	}
}
