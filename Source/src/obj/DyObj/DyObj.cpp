#include "obj/DyObj/DyObj.h"

namespace obj {
	Dyobj::Dyobj(glm::mat4* model, glm::mat4* view, glm::mat4* projection) 
	: Object(model, view, projection) {
		this->_pos = glm::vec3(0.0f, 0.0f, 0.0f);
	}

	Dyobj::~Dyobj() {
		
	}
}
