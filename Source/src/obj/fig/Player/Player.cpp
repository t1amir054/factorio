#include "obj/fig/Player/Player.h"

namespace obj {
	Player::Player(glm::mat4* model, glm::mat4* view, glm::mat4* projection, glm::vec3* camPos) 
	: Figure(model, view, projection) {
		this->_camPos = camPos;
		this->loadTextures();
		this->loadShaders();

		float vertices[] = {
			//position     //texture
			-0.05f, -0.05f,  0.0f, 0.0f,
		 	 0.05f, -0.05f,  1.0f, 0.0f,
			 0.05f,  0.15f,  1.0f, 1.0f,

			-0.05f, -0.05f,  0.0f, 0.0f,
			-0.05f,  0.15f,  0.0f, 1.0f,
			 0.05f,  0.15f,  1.0f, 1.0f
		};

		glGenVertexArrays(1, &this->_VAO);
		glGenBuffers(1, &this->_VBO);
		
		glBindVertexArray(this->_VAO);

		glBindBuffer(GL_ARRAY_BUFFER, this->_VBO);
		glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);

		glEnableVertexAttribArray(0);
		glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, 4 * sizeof(float), (void*)0);

		glEnableVertexAttribArray(1);
		glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 4 * sizeof(float), (void*)(2 * sizeof(float)));

		

		glBindVertexArray(0);
	}

	Player::~Player() {
		glDeleteVertexArrays(1, &this->_VAO);
		glDeleteBuffers(1, &this->_VBO);
		delete this->_shader;
	}

	void Player::draw() {
		this->_shader->use();

		this->update();

		this->_shader->setMat4("model", *this->_model);
		this->_shader->setMat4("view", *this->_view);
		this->_shader->setMat4("projection", *this->_projection);

		this->_shader->setVec2("pos", this->_camPos->x, this->_camPos->y);

		glActiveTexture(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_2D, this->_texture);
		this->_shader->setInt("texture0", 0);

		glBindVertexArray(this->_VAO);
		glDrawArrays(GL_TRIANGLES, 0, 6);
	}

	void Player::update() {

	}

	void Player::loadShaders() {
		this->_shader = new Shader(PLAYER_VERTEX_SHADER_PATH, PLAYER_FRAGMENT_SHADER_PATH);
	}

	void Player::loadTextures() {
		int width, height, nrChannels;
		unsigned char* data;
		glGenTextures(1, &this->_texture);
		glBindTexture(GL_TEXTURE_2D, this->_texture); // all upcoming GL_TEXTURE_2D operations now have effect on this texture object
		// set the texture wrapping parameters
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_BORDER);	// set texture wrapping to GL_REPEAT (default wrapping method)
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_BORDER);
		// set texture filtering parameters
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		// load image, create texture and generate mipmaps
		data = stbi_load("Resources/Player.png", &width, &height, &nrChannels, STBI_rgb_alpha);
		if (data) {
			glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, data);
			glGenerateMipmap(GL_TEXTURE_2D);
		}
		else {
			throw exc::exception("Failed to load texture");
		}
		stbi_image_free(data);
	}
}