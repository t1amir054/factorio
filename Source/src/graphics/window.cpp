#include "graphics/window.h"

namespace ogl {
	float deltaTime = 0.0f;	// Time between current frame and last frame
	float lastFrame = 0.0f; // Time of last frame
	glm::fvec3* PposOffset;

	void gladInit() {
		if (!gladLoadGLLoader((GLADloadproc)glfwGetProcAddress)) {
			throw exc::exception("Failed to initialize GLAD");
		}
	}

	void oglinit() {
		glfwInit();
		glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
		glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
		glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
	}

	void oglTerminate() {
		glfwTerminate();
	}

	void openWindow(GLFWwindow** pwindow, glm::fvec3* posOffset) {
		PposOffset = posOffset;
		GLFWwindow* window = glfwCreateWindow(WIN_WIDTH, WIN_HEIGHT, "LearnOpenGL", NULL, NULL);
		if (window == NULL) {
			glfwTerminate();
			throw exc::exception("Failed to create GLFW window");
		}
		glfwMakeContextCurrent(window);
		*pwindow = window;

		glfwSetFramebufferSizeCallback(window, ogl::framebuffer_size_callback);
		//glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);
		glfwSetScrollCallback(window, scroll_callback);
	}

	// define viewports by given width and height
	void generateViewports(int width, int height) {
		glViewport(0, 0, width, height); //game viewport
	}

	// resize viewports by callback
	void framebuffer_size_callback(GLFWwindow* window, int width, int height) {
		if(window == NULL) {
			throw exc::exception("window is null");
		}
		ogl::generateViewports(width, height);
	}

	void processInput(GLFWwindow* window) {
		float currentFrame = glfwGetTime();
		deltaTime = currentFrame - lastFrame;
		lastFrame = currentFrame;
		const float cameraSpeed = 1.0f * deltaTime; // adjust accordingly

		if(glfwGetKey(window, GLFW_KEY_ESCAPE) == GLFW_PRESS)
			glfwSetWindowShouldClose(window, true);

		if (glfwGetKey(window, GLFW_KEY_W) == GLFW_PRESS)
			PposOffset->y -= cameraSpeed * 1;
		if (glfwGetKey(window, GLFW_KEY_S) == GLFW_PRESS)
			PposOffset->y += cameraSpeed * 1;
		if (glfwGetKey(window, GLFW_KEY_A) == GLFW_PRESS)
			PposOffset->x += cameraSpeed * 1;
		if (glfwGetKey(window, GLFW_KEY_D) == GLFW_PRESS)
			PposOffset->x -= cameraSpeed * 1;
	}

	void scroll_callback(GLFWwindow* window, double xoffset, double yoffset) {
		PposOffset->z += yoffset * 0.1;
		PposOffset->z = glm::clamp<double>(PposOffset->z, -8, -0.5);
	}
}