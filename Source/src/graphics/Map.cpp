#include "graphics/Map.h"

Map::Map(glm::mat4* model, glm::mat4* view, glm::mat4* projection, glm::vec3* position) {
	this->_model = model;
	this->_view = view;
	this->_projection = projection;
	this->_position = position;

	this->_shader = new Shader("Map.vs", "Map.fs");

	this->loadTextures();

//	for(int x = -1; x <= 1; x++)
//		for(int y = -1; y <= 1; y++)
//			this->_chunks.push_back(new Chunk(x, y, this->_shader));

	this->loadChunkByLocation(*this->_position);
}

Map::~Map() {
	delete this->_shader;

	for(size_t i = 0; i < this->_chunks.size(); i++) {
		delete this->_chunks[i];
	}
}

void Map::loadTextures() {
	std::vector<std::string> texNames = {
		"Resources/Dirt.png",
		"Resources/BotLeftEdge.png",
		"Resources/TopLeftEdge.png",
		"Resources/TopRightEdge.png",
		"Resources/BotRightEdge.png",
		"Resources/Water.png"
	};

	int width, height, nrChannels;
	unsigned char* data;
	for(size_t i = 0; i < sizeof(this->_textures) / sizeof(unsigned int); i++) {
		glGenTextures(1, &this->_textures[i]);
		glBindTexture(GL_TEXTURE_2D, this->_textures[i]); // all upcoming GL_TEXTURE_2D operations now have effect on this texture object
		// set the texture wrapping parameters
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_BORDER);	// set texture wrapping to GL_REPEAT (default wrapping method)
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_BORDER);
		// set texture filtering parameters
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		// load image, create texture and generate mipmaps
		data = stbi_load(texNames[i].c_str(), &width, &height, &nrChannels, STBI_rgb_alpha);
		if (data) {
			glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, data);
			glGenerateMipmap(GL_TEXTURE_2D);
		}
		else {
			throw exc::exception("Failed to load texture");
		}
		stbi_image_free(data);
	}
}

void Map::update() {
	this->loadChunkByLocation(*this->_position);
}

void Map::render() {
	this->_shader->use();

	this->_shader->setMat4("model", *this->_model);
	this->_shader->setMat4("view", *this->_view);
	this->_shader->setMat4("projection", *this->_projection);

 	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, this->_textures[0]);
	this->_shader->setInt("texture0", 0);

	glActiveTexture(GL_TEXTURE1);
	glBindTexture(GL_TEXTURE_2D, this->_textures[1]);
	this->_shader->setInt("texture1", 1);

	glActiveTexture(GL_TEXTURE2);
	glBindTexture(GL_TEXTURE_2D, this->_textures[2]);
	this->_shader->setInt("texture2", 2);

	glActiveTexture(GL_TEXTURE3);
	glBindTexture(GL_TEXTURE_2D, this->_textures[3]);
	this->_shader->setInt("texture3", 3);

	glActiveTexture(GL_TEXTURE4);
	glBindTexture(GL_TEXTURE_2D, this->_textures[4]);
	this->_shader->setInt("texture4", 4);

	glActiveTexture(GL_TEXTURE5);
	glBindTexture(GL_TEXTURE_2D, this->_textures[5]);
	this->_shader->setInt("texture5", 5);

	for(size_t i = 0; i < this->_chunks.size(); i++) {
		this->_chunks[i]->draw();
	}
}

void Map::loadChunkByLocation(glm::vec3 position) {
	int r = (int)(glm::sqrt(-position.z) / 2);
	int posx = -(int)(position.x / 3.2f);
	int posy = -(int)(position.y / 3.2f);

	r = r < 1 ? 1 : r;

	printf("\nr: %d\nChunk pos: (%d, %d)\nMy pos: (%f, %f)\nGame pos: (%f, %f)\nzoom: %f\n", r, posx, posy, position.x, position.y, position.x * 10.0f, position.y * 10.0f, -position.z);

	if(this->_chunks.size() != 0)
		this->removeLoadedChunks();

	for(int x = posx - r; x < posx + r; x++)
		for(int y = posy - r; y < posy + r; y++)
			this->_chunks.push_back(new Chunk(x,y, this->_shader));
}

bool Map::isChunkLoaded(int x, int y) {
	for(auto it = this->_chunks.begin(); it != this->_chunks.end(); it++)
		if((*it)->_x == x && (*it)->_y == y)
			return true;
	return false;
}

void Map::removeLoadedChunks() {
	for(size_t i = 0; i < this->_chunks.size(); i++)
		delete this->_chunks[i];
	this->_chunks.clear();
}
