#include "graphics/Chunk.h"

Chunk::Chunk(int x, int y, Shader* shader, int seed) {
	this->_x = x;
	this->_y = y;
	this->_seed = seed;
	this->_shader = shader;

	generate();
	
	glm::vec3 translations[CHUNK_SIZE*CHUNK_SIZE];
	int index = 0;
	glm::vec3 translation;
	for (int y = 0; y < CHUNK_SIZE; y++) {
		for (int x = 0; x < CHUNK_SIZE; x++) {
			translation.x = (float)x / 10.0f;
			translation.y = (float)y / 10.0f;
			translation.z = this->_tiles[(y+CHUNK_SIZE)/2][(x+CHUNK_SIZE)/2];
			translations[index++] = translation;
		}
	}

	// store instance data in an array buffer
	glGenBuffers(1, &this->_instanceVBO);
	glBindBuffer(GL_ARRAY_BUFFER, this->_instanceVBO);
	glBufferData(GL_ARRAY_BUFFER, sizeof(glm::vec3) * CHUNK_SIZE * CHUNK_SIZE, &translations[0], GL_STATIC_DRAW);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	// set up vertex data (and buffer(s)) and configure vertex attributes
	float quadVertices[] = {
		// positions    // texture pos
		 0.00f,  0.10f, 0.0f, 1.0f,
		 0.10f,  0.00f, 1.0f, 0.0f,
		 0.00f,  0.00f, 0.0f, 0.0f,

		 0.00f,  0.10f, 0.0f, 1.0f,
		 0.10f,  0.00f, 1.0f, 0.0f,
		 0.10f,  0.10f, 1.0f, 1.0f
	};
 
	glGenVertexArrays(1, &this->_quadVAO);
	glGenBuffers(1, &this->_quadVBO);
	glBindVertexArray(this->_quadVAO);
	glBindBuffer(GL_ARRAY_BUFFER, this->_quadVBO);
	glBufferData(GL_ARRAY_BUFFER, sizeof(quadVertices), quadVertices, GL_STATIC_DRAW);
	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, 4 * sizeof(float), (void*)0);
	glEnableVertexAttribArray(1);
	glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 4 * sizeof(float), (void*)(2 * sizeof(float)));
	// also set instance data
	glEnableVertexAttribArray(2);
	glBindBuffer(GL_ARRAY_BUFFER, this->_instanceVBO); // this attribute comes from a different vertex buffer
	glVertexAttribPointer(2, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(float), (void*)0);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glVertexAttribDivisor(2, 1);

}

Chunk::~Chunk() {

}

void Chunk::draw() {
	this->_shader->setVec2("chunk", this->_x * 3.2f + 0.05f, this->_y * 3.2f + 0.05f);

	glBindVertexArray(this->_quadVAO);
	glDrawArraysInstanced(GL_TRIANGLES, 0, 6, CHUNK_SIZE * CHUNK_SIZE);
	glBindVertexArray(0);
}

void Chunk::generate(int seed) {
	this->_seed = seed;
	std::string name = std::string("Map/") + (char)(this->_x + '0') + "-" + (char)(this->_y + '0') + ".map";
	std::fstream temp("Map/0-0.map", std::ios_base::in); //change <"0-0.map"> to <name>
	if(!temp.is_open()) {
		throw exc::exception("Cant open .map file");
	}

	std::string str;

	for(size_t i = 0; i < CHUNK_SIZE; i++) {
		std::getline(temp, str);
		for(size_t j = 0; j < CHUNK_SIZE && j < str.size(); j++) {
			this->_tiles[i][j] = (unsigned char)(str[j] - '0');
		}
	}

	temp.close();
}
