#include "exception.h"

namespace exc {
	exception::exception(std::string err, std::string info) {
		this->_err = "Error: " + err + "\n" + info;
	}

	std::string exception::what() const throw() {
		return this->_err;
	}
}