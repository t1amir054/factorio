#include "Header.h"

#include "obj/Object.h"
#include "obj/fig/Figure.h"
#include "obj/fig/Player/Player.h"
#include "graphics/Map.h"

int main() {
	glm::mat4 model;
	glm::mat4 view;
	glm::mat4 projection;

	GLFWwindow* window = nullptr;

	Map* mymap = nullptr;
	obj::Player* player = nullptr;

	try {
		ogl::oglinit();

		glm::fvec3 posOffset(0,0,-3);

		ogl::openWindow(&window, &posOffset);
		ogl::gladInit();
		ogl::generateViewports(WIN_WIDTH, WIN_HEIGHT);

		mymap = new Map(&model, &view, &projection, &posOffset);
		player = new obj::Player(&model, &view, &projection, &posOffset);

		glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);

		while(!glfwWindowShouldClose(window)) {
			ogl::processInput(window);

			glClearColor(0.2f, 0.3f, 0.3f, 1.0f);
 			glClear(GL_COLOR_BUFFER_BIT); 

			//draw:
			model = glm::mat4(1.0f); // make sure to initialize matrix to identity matrix first
			view = glm::mat4(1.0f);
			projection = glm::mat4(1.0f);
			model = glm::rotate(model, glm::radians(0.0f), glm::vec3(1.0f, 0.0f, 0.0f));
			view  = glm::translate(view, posOffset);
			projection = glm::perspective(glm::radians(45.0f), (float)WIN_WIDTH / (float)WIN_HEIGHT, 0.1f, 100.0f);

			mymap->update();
			player->update();

			mymap->render();
			player->draw();

			glfwSwapBuffers(window);
			glfwPollEvents();
		}

	} catch (exc::exception &e) {
		std::cout << e.what() << std::endl;
		getchar();
		return -1;
	}

	delete player;
	delete mymap;

	ogl::oglTerminate();

	return 0;
}
