CCFLAGS = -Ihdrs -I../glad/include -Wall -Werror -Wextra -Wno-unused
GLFLAGS = -lglut -lGLU -lGL -lglfw -lX11 -lpthread -lXrandr -lXi -ldl

OBJ = $(shell find . -type f -name "*.o")

run: build
	./Fuctorio

glad.o:
	gcc -c glad/src/glad.c -o ./bin/glad.o -I./glad/include

compile: glad.o
	make -C Source compile

build: compile glad.o
	g++ ${OBJ} -o ./Fuctorio ${CCFLAGS} ${GLFLAGS}

clean:
	rm ./bin/*.o
	rm ./Fuctorio

generate:
	python3 Map/MapGeneration.py 0
	mv 0-0.map Map

test: generate
	./Fuctorio
