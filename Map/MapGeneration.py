import sys
import math

chunk = []
CAP = 100

def mymap(val, amin, amax, bmin, bmax):
	if amin == amax or bmin == bmax:
		return val
	part1 = (val - amin)/(amax - amin)
	part2 = (bmax - bmin)
	return part1 * part2 + bmin

def func(x, y, seed):
	part1 = math.sin(y * x**2)**2
	part2 = math.cos(x * y**2)**2
	z = part1 - part2
	z = mymap(z, -1, 1, 0, 100)
	return z

#this layer contains the dirt and water placement algorithm
def layer1(seed):
	temp = []
	for x in range(32):
		for y in range(32):
			value = func(x, y, seed)
			if value > CAP:
				temp.append(5)
			else:
				temp.append(0)
		chunk.append(temp)
		temp = []

def writeMap(file):
	for x in range(32):
		for y in range(32):
			file.write(str(chunk[x][y]))
		file.write("\n")

def main(argv):
	if len(argv) != 2:
		print("invalid arguments\nUsage:\n\tMapGeneration.py <seed>")
	seed = int(argv[1])

	layer1(seed)

	filename = "0-0.map"
	for i in range(-5, 5):
		for j in range(-5, 5):
			#filename = str(i) + "-" + str(j) + ".map"
			file = open(filename, "w+")
			writeMap(file)
			file.close()
	
			




if __name__ == "__main__":
	main(sys.argv)
