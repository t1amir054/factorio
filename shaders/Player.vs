#version 330 core

layout (location = 0) in vec2 aPos;
layout (location = 1) in vec2 aTex;

out vec2 Tex;

uniform mat4 model;
uniform mat4 view;
uniform mat4 projection;

uniform vec2 pos;

void main() {
    gl_Position = projection * view * model * vec4(aPos - pos, 0.0, 1.0);
	Tex = aTex;
}