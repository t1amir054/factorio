#version 330 core

layout (location = 0) in vec2 aPos;
layout (location = 1) in vec2 aTex;
layout (location = 2) in vec3 aOffset;

out vec2 tex;
out float texnum;

uniform mat4 model;
uniform mat4 view; 
uniform mat4 projection;

uniform vec2 chunk;

void main() {
	vec2 finalPos = vec2(aPos + aOffset.xy + chunk);
	gl_Position = projection * view * model * vec4(finalPos, 0.0, 1.0);
    tex = aTex;
	texnum = int(aOffset.z);
}