#version 330 core

out vec4 FragColor;

in vec2 tex;
in float texnum;

uniform sampler2D texture0;
uniform sampler2D texture1;
uniform sampler2D texture2;
uniform sampler2D texture3;
uniform sampler2D texture4;
uniform sampler2D texture5;

void main() {
	if(texnum == 0) {
		FragColor = texture(texture0, tex);
	}
	else if (texnum == 1) {
		FragColor = texture(texture1, tex);
	}
	else if (texnum == 2) {
		FragColor = texture(texture2, tex);
	}
	else if (texnum == 3) {
		FragColor = texture(texture3, tex);
	}
	else if (texnum == 4) {
		FragColor = texture(texture4, tex);
	}
	else if (texnum == 5) {
		FragColor = texture(texture5, tex);
	}
}