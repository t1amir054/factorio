#version 330 core

out vec4 FragColor;

uniform sampler2D texture0;

in vec2 aTex;

void main() {
	//FragColor = vec4(0.3, 0.6, 0.55, 1.0);
	FragColor = texture(texture0, aTex);
} 